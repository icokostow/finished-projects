package Resurces;

import org.openqa.selenium.Keys;

import java.sql.Timestamp;

public interface Elements {
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    String JiraURL = "https://sandbox.xpand-it.com/secure/Dashboard.jspa";
    String LOGIN_BUTTON_HOMEPAGE = "//a[@class='aui-nav-link login-link']";
    String USERNAME_FIELD = "//input[@id='login-form-username']";
    String USERNAME = "user24";
    String PASSWORD_FIELD = "//input[@id='login-form-password']";
    String PASSWORD = "0123456";
    String LOGIN_BUTTON = "//input[@id='login']";
    String CREATE_BUTTON = "//a[@id='create_link']";
    String ISSUE_TYPE_FIELD = "//div[@id='issuetype-single-select']//span[@class='icon aui-ss-icon noloading drop-menu']";
    String BUG_ISSUE_TYPE = "//img[@src='https://sandbox.xpand-it.com/secure/viewavatar?size=xsmall&avatarId=10403&avatarType=issuetype']";
    String SUMMARY_FIELD = "//input[@id='summary']";
    String SUMMARY = "The user can't log in with right credentials";
    String DESCRIPTION_FIELD = "//textarea[@id='description']";
    String DESCRIPTION = "When the user wants to log in the system, the system return error 404";
    String PRIORITY_FIELD = "//input[@id='priority-field']";
    String PRIORITY_BUTTON = "//img[@src='/images/icons/priorities/critical.svg']";
    String CREATE_NEWISSUE_BUTTON = "//textarea[@id='description']";
    String SEARCH_FIELD = "//input[@id='quickSearchInput']";
    String ISSUE_LINK = "//span[@class='issue-link-key']";
    String ISSUE_MORE_BUTTON = "//a[@id='opsbar-operations_more']";
    String DELETE_BUTTON = "//aui-item-link[@class='issueaction-delete-issue']";
    String SEARCH_DESC = "right credentials";
    String SUBMIT_DEL_BUTTON = "//div[@id='delete-issue-dialog']//input[@id='delete-issue-submit']";
    String DIALOG_MSG = "//div [@id='delete-issue-dialog']";
    String NO_RESULTS = "//p[@class='no-results-hint']";
}

