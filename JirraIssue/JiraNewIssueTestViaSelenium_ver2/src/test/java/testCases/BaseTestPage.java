package testCases;
import Resurces.Elements;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BaseTestPage implements Elements {
    public WebDriver driver;
    public WebDriverWait wait;

    @BeforeClass
    public static void ClassInit(){
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
    }
    @Before
    public void TestInit() {
        driver = new ChromeDriver();
        driver.get(JiraURL);
        driver.manage().window().maximize();

    }
    @After
    public void TestCleanUp(){
        driver.close();
    }

    public void clickButton(String path){
        WebElement element = driver.findElement(By.xpath(path));
        element.click();
    }

    public void sendText(String path, String text){
        WebElement element = driver.findElement(By.xpath(path));
        element.sendKeys(text);
    }

    public void LoginForm(){
        clickButton(USERNAME_FIELD);
        sendText(USERNAME_FIELD, USERNAME);

        clickButton(PASSWORD_FIELD);
        sendText(PASSWORD_FIELD, PASSWORD);

        clickButton(LOGIN_BUTTON);
    }
    public void ImplicitWait() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    public void Wait(long timeoutMilliSeconds) {
        try {
            Thread.sleep(timeoutMilliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
