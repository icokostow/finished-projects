package testCases;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class JiraCreateIssueTests extends BaseTestPage {

    @Test
    public void test_1_PageNavigatedWhen_JiraPageIsOpened() {
        Assert.assertTrue("The Jira Home Page was not navigated",
                driver.getCurrentUrl().contains(JiraURL));
    }

    @Test
    public void test_2_UserIsNotLoggedIn_inJiraWebApplication() {
        WebElement loginButton = driver.findElement(By.xpath(LOGIN_BUTTON_HOMEPAGE));
        Assert.assertTrue("The user is logged in Jira yet", loginButton.isEnabled());
    }

    @Test
    public void test_3_TheUserIsNotRegisteredInJira() {
        LoginForm();

        Wait(2000);

        WebElement createButton = driver.findElement(By.xpath(CREATE_BUTTON));
        Assert.assertTrue("The user is not registered in Jira software", createButton.isDisplayed());
    }

    @Test
    public void test_4_IssueCreated() {
        LoginForm();

        ImplicitWait();

        sendText(CREATE_BUTTON, "c");

        Wait(2000);

        clickButton(ISSUE_TYPE_FIELD);

        clickButton(BUG_ISSUE_TYPE);

        clickButton(SUMMARY_FIELD);
        sendText(SUMMARY_FIELD, SUMMARY);

        clickButton(DESCRIPTION_FIELD);
        sendText(DESCRIPTION_FIELD, DESCRIPTION);

        clickButton(PRIORITY_FIELD);
        clickButton(PRIORITY_BUTTON);
        sendText(CREATE_NEWISSUE_BUTTON, Keys.ALT + "S");

        Wait(2000);

        WebElement createButton = driver.findElement(By.xpath(CREATE_BUTTON));
        Assert.assertTrue("The new issue was not created", createButton.isEnabled());
    }

    @Test
    public void test_5_FindCreatedIssue() {
        LoginForm();

        Wait(2000);

        clickButton(SEARCH_FIELD);
        sendText(SEARCH_FIELD, SEARCH_DESC);

        WebElement searchFIeld = driver.findElement(By.xpath(SEARCH_FIELD));
        searchFIeld.sendKeys(Keys.ENTER);

        clickButton(ISSUE_LINK);

        Assert.assertTrue("The issue was not found", driver.getCurrentUrl().contains("ALEX"));
    }

    @Test
    public void test_6_DeleteCreatedIssue() {
        LoginForm();

        Wait(2000);

        clickButton(SEARCH_FIELD);
        sendText(SEARCH_FIELD, SEARCH_DESC);

        WebElement searchFIeld = driver.findElement(By.xpath(SEARCH_FIELD));
        searchFIeld.sendKeys(Keys.ENTER);

        clickButton(ISSUE_LINK);

        Wait(1000);

        clickButton(ISSUE_MORE_BUTTON);

        ImplicitWait();

        JavascriptExecutor Scroll = (JavascriptExecutor) driver;
        Scroll.executeScript("window.scrollBy(0,300)", "");

        ImplicitWait();

        clickButton(DELETE_BUTTON);

        ImplicitWait();

        clickButton(SUBMIT_DEL_BUTTON);

        WebElement noResults = driver.findElement(By.xpath(NO_RESULTS));
        Assert.assertTrue("The issue was not deleted", noResults.isDisplayed());
    }
}

